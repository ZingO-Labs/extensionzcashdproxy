use actix_web::{
    http::header,
    middleware::{cors::Cors, Logger},
    web, App, HttpServer,
};

use extensionzcashdproxy::handlers::{handle_results, health};
use extensionzcashdproxy::{loggers, AppState};

macro_rules! create_app {
    (&$logger:expr) => {
        App::new()
            .data(AppState {
                log: $logger.clone(),
            })
            .wrap(Logger::new("%r %b %s"))
            .wrap(
                Cors::new()
                    .send_wildcard()
                    .allowed_origin("https://duckduckgo.com")
                    .allowed_methods(vec!["GET", "POST"])
                    .allowed_header(header::CONTENT_TYPE)
                    .max_age(3600),
            )
            .service(
                web::resource("/process_search_results")
                    .route(web::post().to(handle_results))
                    .route(web::get().to(health)),
            )
    };
}

fn set_env(debug_level: &str) -> slog::Logger {
    std::env::set_var("RUST_LOG", format!("actix_web={}", debug_level));
    match env_logger::try_init() {
        Ok(()) => (),
        Err(logger_error) => log::debug!("{:?}", logger_error),
    };
    loggers::setup_logging()
}

fn get_ip() -> String {
    use std::process::{Command, Stdio};
    let ip_child = Command::new("ip")
        .arg("addr")
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to spawn ip child process!");
    let ip_output = ip_child.stdout.expect("Failed to open stdout");
    let mut grep_child = Command::new("grep")
        .arg("172")
        .stdin(Stdio::from(ip_output))
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to start grep subprocess");
    let result = grep_child
        .wait_with_output()
        .expect("Failed to wait on grep");
    let stringresult = std::str::from_utf8(result.stdout.as_slice()).expect("Couldn't decode!");
    let startindex = stringresult.find("172").expect("Couldn't find 172.");
    let endindex = stringresult.rfind("/16").expect("Couldn't find /16.");
    format!(
        "{}{}",
        stringresult[startindex..endindex].to_string(),
        ":8443"
    )
}

fn main() {
    //tls logic
    use rustls::internal::pemfile::{certs, rsa_private_keys};
    use rustls::{NoClientAuth, ServerConfig};
    let mut config = ServerConfig::new(NoClientAuth::new());
    use std::fs::File;
    use std::io::BufReader;
    let cert_file = &mut BufReader::new(File::open("localhostcert.pem").unwrap());
    let key_file = &mut BufReader::new(File::open("localhostkey.pem").unwrap());
    let cert_chain = certs(cert_file).unwrap();
    let mut keys = rsa_private_keys(key_file).unwrap();
    config.set_single_cert(cert_chain, keys.remove(0)).unwrap();

    //set up logging
    let mylog = set_env("debug");
    let _sys = actix_rt::System::new("ezdpr instance");
    HttpServer::new(move || create_app!(&mylog))
        .bind_rustls(get_ip(), config)
        .unwrap()
        .run()
        .unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::test;
    #[test]
    fn test_process_search_results_service() {
        let mut test_app = test::init_service(create_app!(&set_env("debug")));
        let service_req = test::TestRequest::with_uri("/process_search_results").to_request();
        let resp = test::call_service(&mut test_app, service_req);;
        assert_eq!("HEALTHY!".to_string(), test::read_body(resp));
    }

    #[test]
    #[allow(non_snake_case)]
    fn test_process_search_results_POST() {
        let example_payload = "{\"urls\":[\"hello\"]}";
        use actix_web::http::{header, Method};
        let mut test_app = test::init_service(create_app!(&set_env("debug")));
        let service_req = test::TestRequest::with_uri("/process_search_results")
            .method(Method::POST)
            .header(header::CONTENT_TYPE, "application/json")
            .set_payload(example_payload)
            .to_request();
        let resp = test::read_response(&mut test_app, service_req);
        assert_eq!("[[0,\"hello\"]]", resp);
    }

    #[test]
    fn test_process_search_results_preflight() {
        use actix_web::http::{header, Method};
        let mut test_app = test::init_service(create_app!(&set_env("debug")));
        let service_req = test::TestRequest::with_uri("/process_search_results")
            .method(Method::OPTIONS)
            .header(header::CONTENT_TYPE, "application/json")
            .header(header::ORIGIN, "https://duckduckgo.com")
            .header("Access-Control-Request-Method", "POST")
            .to_request();
        println!("{:?}", service_req);
        let resp = test::read_response(&mut test_app, service_req);
        //assert_eq!(example_payload, test::read_body(resp));
        println!("{:?}", resp);
    }

    #[test]
    fn learn_process_command() {
        println!("{}", get_ip());
    }
}

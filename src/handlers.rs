use actix_web::http::header;
use actix_web::web::{HttpRequest, Json};
use actix_web::{HttpResponse, Responder};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct QueryResults {
    pub urls: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct QueryResult {
    url: String,
    endorsements: u32,
}

#[derive(Debug, Deserialize, Serialize)]
struct EndorsedUrl(u32, String);
fn curate_search_urls(search_urls: &Vec<String>) -> Vec<EndorsedUrl> {
    let mut endorsed: Vec<EndorsedUrl> = vec![];
    let mut unendorsed: Vec<EndorsedUrl> = vec![];
    for url in search_urls.iter() {
        curate_url(url, &mut endorsed, &mut unendorsed);
    }
    endorsed.append(&mut unendorsed);
    endorsed
}

fn curate_url(url: &str, endorsed: &mut Vec<EndorsedUrl>, unendorsed: &mut Vec<EndorsedUrl>) {
    if url == "spam" {
        endorsed.push(EndorsedUrl(1, url.to_string()));
    } else {
        unendorsed.push(EndorsedUrl(0, url.to_string()));
    }
}

pub fn negotiate_options(request: HttpRequest) -> impl Responder {
    HttpResponse::Ok()
        .set_header(header::ACCESS_CONTROL_ALLOW_METHODS, "POST, GET, OPTIONS")
        .set_header("Access-Control-Allow-Origin", "*")
        .set_header("Access-Control-Allow-Headers", "Content-Type")
        .finish()
}

pub fn handle_results(results: Json<QueryResults>) -> impl Responder {
    println!("inside handle_results");
    println!("{:?}", results);
    let curated = curate_search_urls(&results.urls);
    println!("inside handle_results");
    println!("{:?}", curated);
    HttpResponse::Ok()
        .set_header(header::CONTENT_TYPE, "application/json")
        .set_header("Access-Control-Allow-Origin", "https://localhost")
        .json(curated)
}

pub fn health(_req: HttpRequest) -> impl Responder {
    "HEALTHY!".to_string()
}

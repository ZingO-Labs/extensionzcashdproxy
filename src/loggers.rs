use slog_async;
use slog_term;

use slog::{o, Drain, Logger};
use std::fs::OpenOptions;

pub fn setup_logging() -> Logger {
    let log_path = "./RIGHTHERE";
    let file = OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open(log_path)
        .unwrap();

    let decorator = slog_term::PlainDecorator::new(file);
    let drain = slog_term::FullFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();
    slog::Logger::root(drain, o!())
}
